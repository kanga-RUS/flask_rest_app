import json
from flask import abort, Response


def make_error_response(message, status=400):
    return Response(
        response=json.dumps(dict(error=message)),
        status=status, mimetype='application/json'
    )


def get_paginated_list(queryset, schema, url, start, limit):
    count = len(queryset)
    if count < start:
        abort(404)
    obj = {
        'start': start,
        'limit': limit,
        'count': count
    }
    if start == 1:
        obj['previous'] = ''
    else:
        start_copy = max(1, start - limit)
        limit_copy = start - 1
        obj['previous'] = url + '?start=%d&limit=%d' % (start_copy, limit_copy)
    if start + limit > count:
        obj['next'] = ''
    else:
        start_copy = start + limit
        obj['next'] = url + '?start=%d&limit=%d' % (start_copy, limit)
    obj['results'] = schema.dump(queryset[(start - 1):(start - 1 + limit)])
    return obj
