import logging
import sys

from flask import Flask
from flask_restful import Api

from flask_rest_app.extensions import (
    db,
    ma,
    migrate,
)
from flask_rest_app.utils.routes import make_routes


def create_app(config_object="flask_rest_app.settings"):
    """
    :param config_object: The configuration object to use.
    """
    app = Flask(__name__.split(".")[0])
    app.config.from_object(config_object)
    register_extensions(app)
    configure_logger(app)
    register_routes(app)
    return app


def register_routes(app):
    api = Api(app, prefix=app.config['API_PREFIX'])
    make_routes(api)


def register_extensions(app):
    """Register Flask extensions."""
    db.init_app(app)
    migrate.init_app(app, db)
    ma.init_app(app)


def configure_logger(app):
    """Configure loggers."""
    handler = logging.StreamHandler(sys.stdout)
    if not app.logger.handlers:
        app.logger.addHandler(handler)
