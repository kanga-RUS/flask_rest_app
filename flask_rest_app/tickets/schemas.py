from marshmallow import fields
from marshmallow_enum import EnumField

from flask_rest_app.extensions import ma
from .models import Ticket, StatusType, Comment


class TicketSchema(ma.Schema):
    status = EnumField(StatusType)
    email = fields.Email()

    class Meta:
        model = Ticket
        fields = ('id', 'date_created', 'date_modified', 'email', 'text',
                  'theme', 'status')


class CommentSchema(ma.Schema):
    class Meta:
        model = Comment
        fields = ('id', 'ticket_id', 'text', 'email', 'date_created')
