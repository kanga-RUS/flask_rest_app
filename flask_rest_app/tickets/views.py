from flask import jsonify, request
from flask_restful import Resource
from marshmallow import ValidationError

from flask_rest_app.extensions import db
from flask_rest_app.tickets.models import Comment, StatusType, Ticket
from flask_rest_app.tickets.schemas import CommentSchema, TicketSchema
from flask_rest_app.utils.controllers import (get_paginated_list,
                                              make_error_response)
from ..settings import API_PREFIX

comment_schema = CommentSchema()
comments_schema = CommentSchema(many=True)
ticket_schema = TicketSchema()
tickets_schema = TicketSchema(many=True)


class TicketsController(Resource):
    url = f"{API_PREFIX}/tickets"

    def post(self):
        if request.is_json:
            json_data = request.get_json()
            if not json_data:
                return make_error_response("No input data provided")
            try:
                data = ticket_schema.load(json_data)
                ticket = Ticket(
                    email=data['email'],
                    theme=data['theme'],
                    text=data['text']
                )
                db.session.add(ticket)
                db.session.commit()
                return ticket_schema.dump(ticket), 201
            except ValidationError as err:
                return make_error_response(str(err))
        else:
            return make_error_response(
                "The request payload is not in JSON format"
            )

    def get(self):
        tickets = Ticket.query.all()
        return jsonify(get_paginated_list(
            tickets,
            tickets_schema,
            self.url,
            start=int(request.args.get('start', 1)),
            limit=int(request.args.get('limit', 2))
        ))


class TicketController(Resource):
    def get(self, pk):
        ticket = Ticket.query.get_or_404(pk)
        return ticket_schema.dump(ticket), 200

    def put(self, pk):
        if request.is_json:
            json_data = request.get_json()
            if not json_data:
                return make_error_response("No input data provided")
            ticket = Ticket.query.filter_by(id=pk).one_or_none()
            if ticket is None:
                return make_error_response('Ticket not found', status=404)
            data = ticket_schema.load(json_data)
            current_status = ticket.status
            new_status = data.get('status')
            if new_status:
                if current_status == StatusType.closed:
                    return make_error_response(
                        "Ticket closed. You can't modify it"
                    )
                elif (
                        current_status == StatusType.open and
                        new_status in (StatusType.answered,
                                       StatusType.closed,
                                       StatusType.wip)
                ) or (
                        current_status == StatusType.answered and
                        new_status in (StatusType.wip, StatusType.closed)
                ):
                    ticket.status = new_status
                    db.session.commit()
                    resp = ticket_schema.dump(ticket)
                    print('AFTER commit', resp)
                    return resp, 202
            return ticket_schema.dump(ticket), 304
        # return make_error_response("The request payload is not in JSON format")


class TicketComments(Resource):
    url = f"{API_PREFIX}/ticket/" + "{}/comments"

    def get(self, pk):
        comments = Comment.query.filter_by(ticket_id=pk).order_by(
            Comment.date_created.desc()
        ).all()
        return jsonify(get_paginated_list(
            comments,
            comments_schema,
            self.url.format(pk),
            start=int(request.args.get('start', 1)),
            limit=int(request.args.get('limit', 2))
        ))


class CommentController(Resource):
    def post(self):
        if request.is_json:
            json_data = request.get_json()
            if not json_data or not json_data.get('ticket_id'):
                return make_error_response("No input data provided")
            try:
                data = comment_schema.load(json_data)
                ticket = Ticket.query.filter_by(
                    id=data['ticket_id']
                ).one_or_none()
                if ticket is None:
                    return make_error_response("Ticket not found", status=404)
                if ticket.status == StatusType.closed:
                    return make_error_response(
                        "Ticket closed. You can't add a comment"
                    )
                comment = Comment(
                    ticket_id=data['ticket_id'],
                    email=data['email'],
                    text=data['text']
                )
                db.session.add(comment)
                db.session.commit()
                return comment_schema.dump(comment), 201
            except ValidationError as err:
                return make_error_response(str(err))
        return make_error_response("The request payload is not in JSON format")
