# -*- coding: utf-8 -*-
"""Defines fixtures available to all tests."""

import pytest

from flask_rest_app.app import create_app
from flask_rest_app.extensions import db as _db


@pytest.fixture(scope='session')
def client(request):
    app = create_app('tests.settings')
    with app.test_client() as client:
        ctx = app.app_context()
        ctx.push()
        yield client
        ctx.pop()


@pytest.fixture(scope='session')
def db(client):
    _db.drop_all()
    _db.create_all()
    yield _db
    _db.session.remove()
    _db.drop_all()

    return _db
