FROM python:latest

WORKDIR /usr/src/

RUN apt-get update && apt-get -y install libpq-dev gcc libssl-dev

COPY . .

RUN pip install -r requirements.txt

ENTRYPOINT ["sh", "./entrypoint.sh"]
