# flask_rest_app

Тестовое задание тикет-система для демострации работы API.
Реализовано на Python, Flask, PostgreSQL, SQLAlchemy, uWSGI, Docker, pytest


Запуск приложения

```bash
docker-compose up --build
```

## Примеры запросов

1. POST-запрос для создания тикета
```bash
curl -d '{
    "theme":"for testing", 
    "email":"123@example.com", 
    "text": "some text here"
}' -H "Content-Type: application/json" -X POST http://localhost:5000/api/v1/tickets
```
Response
```json
{
    "date_modified": "2021-08-25T11:05:46", 
    "id": 1, 
    "text": "some text here", 
    "date_created": "2021-08-25T11:05:46", 
    "theme": "for testing", 
    "email": "123@example.com", 
    "status": "open"
}
```
2. GET-запрос для получения одного тикета
```bash
curl -H "Content-Type: application/json" -X GET http://localhost:5000/api/v1/ticket/1
```
Response
```json
{
    "date_modified": "2021-08-25T11:05:46",
    "id": 1,
    "text": "some text here",
    "date_created": "2021-08-25T11:05:46",
    "theme": "for testing",
    "email": "123@example.com",
    "status": "open"
}
```
3. GET-запрос для получения списка тикетов
```bash
curl -H "Content-Type: application/json" -X GET http://localhost:5000/api/v1/tickets
```
Response
```json
{
    "count": 3,
    "limit": 2,
    "next": "/api/v1/tickets?start=3&limit=2",
    "previous": "",
    "results": [
        {
            "date_created": "2021-08-25T11:05:46",
            "date_modified": "2021-08-25T11:05:46",
            "email": "123@example.com",
            "id": 1,
            "status": "open",
            "text": "some text here",
            "theme": "for testing"
        },
        {
            "date_created": "2021-08-25T11:09:07",
            "date_modified": "2021-08-25T11:09:07",
            "email": "hello@example.com",
            "id": 2,
            "status": "open",
            "text": "some text here",
            "theme": "new one ticket"
        }
    ],
    "start": 1
}
```
Дополнительно можно передавать параметры limit и start
```bash
curl -H "Content-Type: application/json" -X GET http://localhost:5000/api/v1/tickets?limit=4&start=2
```
Response
```json
{
    "count": 3,
    "limit": 4,
    "next": "",
    "previous": "/api/v1/tickets?start=1&limit=1",
    "results": [
        {
            "date_created": "2021-08-25T11:09:07",
            "date_modified": "2021-08-25T11:09:07",
            "email": "hello@example.com",
            "id": 2,
            "status": "open",
            "text": "some text here",
            "theme": "new one ticket"
        },
        {
            "date_created": "2021-08-25T11:10:08",
            "date_modified": "2021-08-25T11:10:08",
            "email": "hello@example.com",
            "id": 3,
            "status": "open",
            "text": "text ticket body",
            "theme": "third ticket"
        }
    ],
    "start": 2
}
```
4. POST-запрос для добавления комментария к тикету
```bash
curl -d '{
    "ticket_id": 1, 
    "text":"comment for first ticket", 
    "email": "alex@example.com"
}' -H "Content-Type: application/json" -X POST http://localhost:5000/api/v1/comments
```
Response
```json
{
    "ticket_id": 1,
    "id": 1,
    "text": "comment for first ticket",
    "date_created": "2021-08-25T11:17:40",
    "email": "alex@example.com"
}
```
5. GET-запрос для получения комментариев к тикету
```bash
curl -H "Content-Type: application/json" -X GET http://localhost:5000/api/v1/ticket/1/comments
```
Response
```json
{
    "count": 1,
    "limit": 2,
    "next": "",
    "previous": "",
    "results": [
        {
            "date_created": "2021-08-25T11:17:40",
            "email": "alex@example.com",
            "id": 1,
            "text": "comment for first ticket",
            "ticket_id": 1
        }
    ],
    "start": 1
}
```
Дополнительно можно передавать параметры limit и start
6. PUT-запрос для изменения статуса тикета
```bash
curl -d '{
    "date_modified": "2021-08-25T11:05:46", 
    "id": 1, 
    "text": "some text here", 
    "date_created": "2021-08-25T11:05:46", 
    "theme": "for testing", 
    "email": "123@example.com", 
    "status": "close"
}' -H "Content-Type: application/json" -X PUT http://localhost:5000/api/v1/ticket/1
```
Response
```json
{
    "date_modified": "2021-08-25T11:05:46",
    "id": 1,
    "text": "some text here",
    "date_created": "2021-08-25T11:05:46",
    "theme": "for testing",
    "email": "123@example.com",
    "status": "close"
}
```
